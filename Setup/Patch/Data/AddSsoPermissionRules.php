<?php

namespace GetNoticed\Ibl\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use GetNoticed\ImprovedBackendLogin\Model\SsoPermissionFactory;
use GetNoticed\ImprovedBackendLogin\Model\ResourceModel\SsoPermission as SsoPermissionResource;
use GetNoticed\ImprovedBackendLogin\VO\WhitelistDomainObject;

class AddSsoPermissionRules implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var SsoPermissionFactory
     */
    private $permissionFactory;

    /**
     * @var SsoPermissionResource
     */
    private $permissionResource;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        SsoPermissionFactory $permissionFactory,
        SsoPermissionResource $permissionResource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->permissionFactory = $permissionFactory;
        $this->permissionResource = $permissionResource;
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->addSsoPermissionRules();
        $this->moduleDataSetup->endSetup();
    }

    protected function addSsoPermissionRules(): void
    {
        /** @var \GetNoticed\ImprovedBackendLogin\Model\SsoPermission $permissionGn */
        $permissionGn = $this->permissionFactory->create();
        $currentDt = new \DateTime();
        $permissionGn
            ->setProviderCode('google')
            ->setAdminRoleId(1)
            ->setWhitelistDomains(
                (new WhitelistDomainObject())
                    ->setUser('*')
                    ->setHostname('getnoticed')
                    ->setTld('nl')
            )
            ->setCreatedAt($currentDt)
            ->setUpdatedAt($currentDt);

        $this->permissionResource->save($permissionGn);
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
